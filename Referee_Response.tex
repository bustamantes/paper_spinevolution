\documentclass[a4,useAMS,usenatbib,usegraphicx,12pt]{article}
%External Packages and personalized macros
\include{latex/macros_response}

\setlength\parindent{20pt}
\setlength\parskip{4pt}

\title{Response to referee on: \\ \textit{Spin evolution and feedback of supermassive black holes in cosmological simulations}}
\author{}
\date{}
  
\begin{document}
\maketitle
First of all, we would like to thank the referee for the insightful and constructive comments that have helped us to improve the paper. Below, we first address the major comments and revisions suggested by the referee. After this, we address the more minor referee comments point by point.

\section{Main discussion item}

The referee's main objection to our paper lies in our conjecture to use the spin as a trigger for the transition between the quasar and jet mode (and hence the BH feedback), which he argues is physically not well enough motivated and in contraction with the physics of accretion disks. We respectfully disagree with this assessment, and would like here to explain why. We do this by responding directly to the central points of the criticism raised by the referee (reproduced in italics), and by expanding further on why we think our approach is interesting to examine, despite its admittedly large uncertainties.
\ \\

\emph{The transition between the quasar and jet mode of BH feedback is usually set up based on the Eddington ratio. First of all I want to insist that such a fixed Eddington ratio is suggested both by observations (e.g. Russell et al. 2013; and references therein) and accretion disc physics, but new theoretical models are always welcome when properly motivated.}

We agree that a fixed Eddington ratio is often used for parameterizing the transition, based on the fact that bright quasar are seen with high Eddington ratio and radio AGN with low Eddington ratio (as for example shown by Russel et al.). However, we note that whether this dichotomy is equally valid for low mass systems at identical Eddington ratio is observationally not clear. We are also not aware of a firm theoretical basis for the assumption of a fixed Eddington ratio, given that the very existence of two separate feedback modes, schematically referred to as quasar vs kinetic/jet mode, is not even established unambiguously from a theoretical point of view.

We also think that at this stage it is fundamentally unclear whether the Eddington ratio is causal for the transition, or whether a bimodal Eddington ratio distribution simply arises as a consequence of the fact that there are two feedback modes of different strength. In particular, there are at least two different scenarios for how the quenching transition could arise.

In the first scenario, the radiative (quasar) feedback that operates in highly accreting BHs is eventually able to induce quenching by driving strong gas outflows. The subsequent drop in the Eddington ratio may then be associated with a transition to the kinetic feedback mode. While here the Eddington ratio is a key factor for marking the transition, it is *not* the trigger for the quenching. This scenario was for example explored in the Illustris simulation, where a fixed Eddington ratio was used for the transition.

Alternatively, quenching and strong gas outflows may instead arise mainly from the kinetic feedback mode.  This is the scenario proposed by Weinberger+2017 for the IllustrisTNG model, and which is also followed in our simulations in this paper. Here, the transition from thermal quasar mode to kinetic/jet mode is playing a central role as it triggers quenching, but this switch does in principle not need to be related to the Eddington ratio at all. This is because regardless of whether such a connection is invoked or not, due to the high efficiency of the kinetic BH feedback, the outcome will typically be consistent with the observational inference that AGN with low Eddington ratio are in the kinetic mode, and vice versa. This is because of the high efficiency of the kinetic mode in suppressing gas accretion and quickly pushing the BH into a self-regulated low accretion state (the kinetic energy thermalises in a timescale of just 0.5 Myr; Weinberger+2017). We also note that Weinberger+(2017) abandoned a fixed Eddington ratio for the transition switch in favor of a Eddington ratio transition threshold that effectively rises with BH mass. This was done on purely heuristic grounds,  because for a fixed Eddington ratio low-mass BHs in small galaxies would frequently enter the jet mode, causing both sluggish growth of these BHs and an insufficiently expressed bimodality in the galaxy properties. 

Despite this, the Eddington ratio distributions obtained with the IllustrisTNG model are consistent with the observations the referee points out. Further results along these lines can also be found in the recent paper by Terrazas+2019, who ran simulations in which the different parameters of the feedback switch of Weinberger+2017 are varied. They show that the used  kinetic feedback always leads to a self-regulated cycle and a strong drop of the accretion rate.  In all their simulations, the average accretion rates of BHs in the quasar and kinetic modes are always roughly $10^{-2}\, M_{\mathrm{Edd}}$ and $10^{-4} - 10^{-5}\, M_{\mathrm{Edd}}$ at $z=0$, respectively, despite using a non-constant Eddington ratio switch.

Likewise, the outcome of our new spin-dependent switch is also consistent with the Eddington-ratio bimodality. This is seen in the enclosed Figure~\ref{Fig:Fig1}, where we show the diagram of Eddington ratio vs black hole mass for BHs at redshifts $z\leq 1$ in the fiducial run with the Weinberger+2017 switch (left panel), and in the run with our self-gravity transition (right panel). The colour in the right panel represents the accretion state. Both distributions are very similar, indicating that the bimodality can also be reproduced with a transition that is primarily sensitive to BH mass and spin. We note that in our model, all black holes in the chaotic regime end up with low accretion rates, and thus the quasar and kinetic feedback modes are consistently associated to high and low accretion modes.  This directly shows that there are multiple ways of arriving at this outcome, and that the interpretation of the observational data as providing evidence that  a fixed Eddington ratio governs the transition is not compelling.
\ \\

\emph{The authors argue that this transition can be driven by the self-gravity of the disc itself: once the
  self-gravity radius becomes smaller than the warp radius, accretion proceed in a chaotic fashion and is associated with kinetic feedback. This is where the problem lies.  Assuming the accretion disc follows the physics of Shakura \& Sunyaev accretion disc, self-gravity of the disc follows a -8/27 power law and the warp radius a -1/4 power law for Eddington ratio (coined $f$). Hence, the $R_{sg}/R_{warp}$ ratio is proportional to the Eddington ratio with a power of -5/108. The authors claim that: ``Following Weinberger et al. (2017), we associate the quasar thermal feedback mode with the coherent regime (high accretion mode), and the kinetic feedback mode with the self-gravity regime (low accretion mode).''   However it is mathematically clear that all other dependencies kept fixed (BH mass, spin, etc.), it is exactly the opposite that is expected: high-accretion rates (large $f$)  associated with chaotic accretion and low accretion rates (low f) associated with coherent accretion (and Fig. 4 shows exactly this). Obviously, the dependency of $R_{sg}/R_{warp}$ with $f$ is extremely insignificant but this is definitely saying something about the underlying physics justified for this new model.}

We agree that our transition to the chaotic mode is not a strong function of the Eddington ratio. In fact, as the referee points out, it only extremely weakly depends on it, even with a small negative power. But considering this dependence at fixed BH mass and spin is misleading because the dependencies on these two quantities are far stronger than on the Eddington ratio, and are completely dominating the behavior for this reason. It is exactly the strong dependence on BH mass (for high spin), and the coincidence that this predicts the transition to happen at the BH mass scale where quenching is seen observationally, that makes this criterion a suggestively natural place for conjecturing that here the accretion flow may not only become chaotic but also induce a transition in the feedback impact of the BH. We do not mean to claim that this is capturing the physics at a deep level (it is admittedly not, and the small-scale physics of accretion discs is by no means resolved in our cosmological simulations anyhow), but it is ultimately not more ad-hoc as postulating a fixed Eddington ratio, and arguably less contrived than the original model by Weinberger+(2017). We therefore view this as an interesting scenario to consider, at least in a small part of our paper, as we do.


\ \\

\emph{ In addition, the Toomre parameter for the low accretion regime cannot be that of the alpha-disc and since the effective sound speed of the disc is supposed to increase and the surface density to decrease, the low accretion rate regime is conspiring to increase the stability of the disc, and makes the disc less likely self-gravitating, in contradiction with the new model deployed in this paper.}

We agree with the referee that it doesn't make sense to apply the Toomre criterion to the low accretion state. However, we also do not apply it in this regime.
The spin evolution model and the assumption of a thin $\alpha$ disc solution are only valid if the accretion rate is above $10^{-3}\, M_{\mathrm{Edd}}$ (see first paragraph of section 2.1 of the paper \footnote{There was a typo in the original paper, and we wrote $1\%$ instead of $0.1\%$. This has been now corrected.}). We do not propose self-gravity driven chaotic accretion as the mechanism through which low-Eddington accreting discs operate, but instead, as the triggering criterion for inducing the transition to a low-Eddington ratio flow.


Given the above criticism by the referee, we have rephrased the subsection 3.2 of the paper to provide a better justification and explanation of our transition switch, along the discussion above.


%.........................................................................
\begin{figure}[htbp]
\centering
\includegraphics[width=1\textwidth]{./figures/Figure1_Referee.png}
\caption{Distribution of Eddington ratio vs black hole mass in the fiducial model (based on TNG), and a model using the new transition switch proposed here.
\label{Fig:Fig1}}
\end{figure}
%.........................................................................



\section{More minor comments }

\begin{itemize}
\item \textit{Fanidakis et al. (2011) pioneered such treatments in the context of semi-analytic modelling of galaxy formation.} Fanidakis did not pioneer the BH spin treatment in SAM. There has been a couple of papers before him: Volonteri+05, Lagos+09. Cite also Barausse 2012.

    \textbf{Reply:} Agreed. References added.

\item \textit{Dubois et al. (2014) study the BH spin evolution in hydrodynamical cosmological simulations in a post-processing fashion.} In addition, Dubois et al. (2014, 440, 2333) modelled self-consistently BH spin in cosmological hydrodynamical simulations.

    \textbf{Reply:} Agreed. Reference added.

\item When introducing the recoil kick velocity, this must be said right-away (not wait for section 3.1.2) that these kicks are only used as diagnostics and not self-consistently used by the simulation (due to BH repositioning).

    \textbf{Reply:} Done. Now we briefly discuss this at the end of subsection 2.2.

\item The BHL accretion rate includes the contribution from relative BH-gas velocity, which does not appear here due to ad-hoc BH repositioning. This must be said near equation 26.

    \textbf{Reply:} We have clarified this in a footnote right after equation 26.

\item BH seeding based on local baryon properties has also been employed early on in Dubois+10. A much more realistic model than those already cited, and also employed in full large-scale cosmological simulations, is based on popIII star seeding by Habouzit+17.

    \textbf{Reply:} References added.

\item \textit{Weinberger et al. (2017) associate these types of accretion with two different BH feedback modes, namely a thermal quasar feedback mode that is activated during the high accretion state, and a kinetic feedback mode that operates in BHs with low-accreting BHs.} And Dubois+12 before him.

    \textbf{Reply:} Agreed. Reference added.

\item \textit{For instance, Sijacki et al. (2015) and other previous works have used a fixed threshold value of the fraction $\chi \equiv \dot{M}_{bh}/\dot{M}_{\mathrm{Edd}}$, i.e. when $\chi<\chi_{\mathrm{th}}$ the low accretion mode is adopted, otherwise the BH is assumed to be in a high accretion state.} Add that these mode decomposition is based on the underlying accretion disc physics and observations with appropriate citations.

    \textbf{Reply:} A brief motivation of the use of a $\chi$ threshold and some references were added.

\item Equation 30 is not used in the model (section 3 is called \textit{BH model}) and might lead a \textit{distracted} reader to believe that such a spin-dependent parametrisation of jet feedback is used. I suggest to move it to a more appropriate section where the impact of jet feedback and this possible spin-dependency is discussed.

    \textbf{Reply:} We deleted this equation from the text. Instead, we added an in-line equation with the dependence of jet power on BH spin.

\item \textit{An important example for the outcome of this is the strong galaxy colour bimodality in IllustrisTNG, which is remarkably consistent with observations (Nelson et al. 2018). This represents a major improvement with respect to the first generation of Illustris simulations, in which the galaxy colour bimodality was much weaker (Nelson et al. 2015).} Since this is said, it might be worth quickly explaining why there is such a difference between Illustris and IlustrisTNG, i.e. is the revamped AGN model responsible for this difference?

    \textbf{Reply:} We added an extra sentence with this explanation. The weak galaxy colour bimodality in the first generation of Illustris is indeed caused by the inability of the BH feedback to quench residual star formation in massive haloes.

\item \textit{Given that galaxy mergers trigger starbursts and drive morphological changes, and that post-merger galaxies tend to have a quiescent star forming activity, the inability to produce starbursts might naturally lead to a lacking connection between galaxy morphology and colour.} Note that this is a matter of debate. Observed merging galaxies at high redshift (z=1-2) caught in the act show on average only a moderate increase of star formation (Rodighiero+11; Stott+13; Lofthouse+17), while low redshift galaxies are much more enhanced. This redshift-dependency (actually gas-richness) is supported by theoretical work (Fensch+17). It is very debated whether the population of starbursts is internally driven -- the galaxy is gas-rich, or positive AGN feedback (e.g. Gaibler+12; Bieri+16) -- or externally driven -- mergers or more widely external perturbations trigger starbursts--. I suggest to reformulate in a more balanced way.

    \textbf{Reply:} As suggested, we reformulated this paragraph in more balanced way. We also included additional references.

\item \textit{Nonetheless, our approach does not require calibration, and self-consistently predicts the activation of the kinetic feedback mode based only on the physical properties of BHs, which represents a big improvement with respect to previous ad-hoc schemes.} To me this new kinetic/quasar switch based on self-gravity of the accretion disc (in a wrong way as discussed early in this report) seems much more ad-hoc than the previous schemes employed in the literature, which are based on the actual physics of accretion discs and observational evidence (e.g. Russell+13). Please, reformulate.

    \textbf{Reply:} See main discussion item.

\item The difference in evolutionary path sketched by the grey arrow in Fig. 4 is justified by the supposedly stronger impact of the kinetic mode of AGN feedback w.r.t. the quasar mode, i.e. for a given accretion rate onto a BH with a given distribution of gas, the kinetic feedback has more impact on the BH self-regulation (and the ejection of gas) than the quasar mode. This is nowhere shown or justified. Can you clarify this point?

    \textbf{Reply:} This is due to the higher efficiency of the kinetic feedback mode compared to the quasar thermal mode (see e.g. Fig. 6 of Weinberger+17). We added a sentence with this clarification and the corresponding reference (paragraph 6, subsection 3.2).

\item \textit{[...] the N-body magneto-hydrodynamics moving-mesh [...]}. Remove magneto, unless the simulations are run with magnetic fields (and in that case explain how B is set up, evolved, etc.).

    \textbf{Reply:} A brief explanation of the set up and evolution of B was added in subsection 4.1

\item Add Horizon-AGN (Dubois+14) to the list of \textit{state-of-art} cosmological simulations.

    \textbf{Reply:} Done.

\item \textit{We note that during the self-gravity regime, $j_{\mathrm{gas},z}$ appears to display a discontinuous behaviour. This is, however, a spurious effect due our recording of all relevant BH properties only when the BH spin is modified, which is done for computational reasons}. This is strange onto why this should be expensive to compute the gas properties at all time steps given that it is very often that gas neighbours are required for many aspects of subgrid physics (star formation, feedback, etc.). At least, this part of the $j_{\mathrm{gas},z}$ curve should be highlighted in dashed, or anything that gives a different meaning to this regime, where the sharp variations in $j_{\mathrm{gas},z}$ cannot be trusted (and this should be said in the caption as well). Note that Dubois, Volonteri \& Silk 14 showed that, indeed, the gas AM is very quickly re-oriented for these low-redshift massive BHs (see their Fig.1 similar to this Fig. 5).

    \textbf{Reply:} Dashed lines are now shown and a description is added in the caption of Figure 5.

\item \textit{Approximately at this mass scale, the kinetic feedback mode kicks in as well, which limits gas accretion and leaves BH coalescence as the only effective spin evolution channel.} Is the BH accretion limited by the feedback because it is kinetic -- and, hence, explain why it should lead to more efficient self-regulation--, or is it because there is a sharp transition from low-mass highly accreting BHs to high-mass lowly accreting BHs due to galaxy intrinsic properties (e.g. gas fraction in galaxies, gas temperatures) at this particular mass scale?

    \textbf{Reply:} It has been already discussed in the previous sections of the paper and in the referenced method papers (e.g. Weinberger+2017, Nelson+2018) that the transition in galaxy properties and the accretion state of the BH is caused by the high efficiency of the kinetic mode compared to the thermal quasar mode ($\epsilon_{\mathrm{f,kin}}=0.1$ compared to $\epsilon_{\mathrm{r}}(a)\epsilon_{\mathrm{f,high}}\simeq 0.02$) and not by environmental or intrinsic galaxy properties. We included again this explanation, together with the reference to Weinberger+2017, in the second paragraph of subsection 5.2.

\item Caption of Fig. 7: Recall that the parameter k controls the anisotropy of the accretion when the self-gravity of the disc is met.

    \textbf{Reply:} Done.

\item Figures 8 and 9: shaded areas and lines lie outside of the panels. Fix that.

    \textbf{Reply:} Done.

\end{itemize}

\end{document}
