Things to do
=================================== 

- Write theory part of the paper.
- Implement improved dynamical friction treatment as Dubois et al. 2014.
- Test new dynamical friction treatment.
- Correct spin evolution equations.
- Further test of spin evolution. Cosmological run and isolated galaxy.
- Run either high-resolution cosmological box and/or Auriga-like simulation.
- Write analysis part and rest of paper
